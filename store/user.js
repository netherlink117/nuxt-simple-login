export const state = () => ({
  name: process.broswer ? sessionStorage.getItem('name') || null : null,
  token: null,
})

export const mutations = {
  setName(state, name) {
    state.name = name
    if (process.browser) sessionStorage.setItem('name', state.name)
  },
  setToken(state, token) {
    state.token = token
  },
}
