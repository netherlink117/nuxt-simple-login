const { resolve } = require('path')
const { SignJWT } = require('jose/jwt/sign')
const bcrypt = require('bcrypt')
const db = require(resolve(__dirname, 'database/db.js'))

const singin = function (req, res, next) {
  if (
    req.body.password === null ||
    req.body.password === undefined ||
    req.body.password === '' ||
    req.body.name === null ||
    req.body.name === undefined ||
    req.body.name === ''
  ) {
    res.statusCode = 400
    res.end(JSON.stringify({ message: 'All the fields are required.' }))
  } else {
    db('users')
      .where({ name: req.body.name.toLowerCase() })
      .select('id', 'name', 'password', 'created_at', 'updated_at')
      .then((rows) => {
        if (rows.length > 0) {
          if (bcrypt.compareSync(req.body.password, rows[0].password)) {
            new SignJWT({ 'http://localhost:3000/signin': true })
              .setProtectedHeader({ alg: 'HS256' })
              .setIssuedAt()
              .setIssuer('http://localhost:3000')
              .setSubject(
                (req.headers['x-forwarded-for'] || '').split(',')[0] ||
                  req.connection.remoteAddress
              )
              .setAudience('http://localhost:3000')
              .setExpirationTime('1h')
              .setNotBefore(Math.floor(Date.now() / 1000))
              .setJti(rows[0].id)
              .sign(
                Buffer.from(
                  '7f908df6c8bd634f769c073a48986d77677b79bc6aa19b106f976f2db18d38c2',
                  'hex'
                )
              )
              .then((token) => {
                res.end(JSON.stringify({ token }))
              })
              .catch((err) => {
                res.statusCode = 500
                res.end(JSON.stringify({ message: err.message }))
              })
          } else {
            res.statusCode = 403
            res.end(JSON.stringify({ message: 'Wrong credentials.' }))
          }
        } else {
          res.statusCode = 403
          res.end(JSON.stringify({ message: 'Wrong credentials.' }))
        }
      })
      .catch((err) => {
        res.statusCode = 500
        res.end(JSON.stringify({ message: err.message }))
      })
  }
}

module.exports = {
  handler(req, res, next) {
    if (req.method === 'POST') singin(req, res, next)
  },
}
