const { jwtVerify } = require('jose/jwt/verify')

const authenticate = async function (req, res, next) {
  if (
    req.headers.token !== null &&
    req.headers.token !== undefined &&
    req.headers.token !== ''
  ) {
    try {
      const { payload } = await jwtVerify(
        req.headers.token,
        Buffer.from(
          '7f908df6c8bd634f769c073a48986d77677b79bc6aa19b106f976f2db18d38c2',
          'hex'
        ),
        {
          algorithms: ['HS256'],
          issuer: 'http://localhost:3000',
          subject:
            (req.headers['x-forwarded-for'] || '').split(',')[0] ||
            req.connection.remoteAddress,
          audience: 'http://localhost:3000',
        }
      )
      req.user = payload.jti
    } catch (err) {
      res.tokenError = err.message
    }
  }
}

module.exports = {
  async handler(req, res, next) {
    req.user = null
    await authenticate(req, res, next)
    next()
  },
}
