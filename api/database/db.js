const { resolve } = require('path')
const knex = require('knex')
const knexfile = require(resolve(__dirname, 'knexfile.js'))
module.exports = knex(
  process.env.NODE_ENV !== 'production'
    ? knexfile.development
    : knexfile.production
)
