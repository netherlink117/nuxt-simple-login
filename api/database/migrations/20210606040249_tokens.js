exports.up = function (knex) {
  return knex.schema.createTable('tokens', function (table) {
    table.integer('user_id').unsigned()
    table
      .foreign('user_id')
      .references('users.id')
      .onDelete('cascade')
      .onUpdate('cascade')
    table.string('token_string').notNullable()
    table.integer('created_at')
    table.integer('updated_at')
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('tokens')
}
