exports.up = function (knex) {
  return knex.schema.createTable('users', function (table) {
    table.increments()
    table.string('name').notNullable()
    table.string('password').notNullable()
    table.integer('created_at')
    table.integer('updated_at')
    table.unique('name')
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('users')
}
