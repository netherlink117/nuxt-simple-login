const { resolve } = require('path')
const bcrypt = require('bcrypt')
const db = require(resolve(__dirname, 'database/db.js'))

const singup = function (req, res, next) {
  if (
    req.body.password === null ||
    req.body.password === undefined ||
    req.body.password === '' ||
    req.body.name === null ||
    req.body.name === undefined ||
    req.body.name === ''
  ) {
    res.statusCode = 400
    res.end(JSON.stringify({ message: 'All the fields are required.' }))
  } else {
    db('users')
      .insert({
        name: req.body.name.toLowerCase(),
        password: bcrypt.hashSync(req.body.password, 7),
        created_at: Date.now(),
      })
      .then((rows) => {
        if (rows[0] !== undefined) {
          res.end(JSON.stringify({ message: 'Success.' }))
        } else {
          res.statusCode = 403
          res.end(JSON.stringify({ message: 'Error.' }))
        }
      })
      .catch((err) => {
        res.statusCode = 500
        res.end(
          JSON.stringify({
            message:
              err.errno === 19
                ? 'Name not available.'
                : `Error code: ${err.errno}.`,
          })
        )
      })
  }
}

module.exports = {
  handler(req, res, next) {
    if (req.method === 'POST') singup(req, res, next)
  },
}
