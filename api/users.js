const { resolve } = require('path')
const db = require(resolve(__dirname, 'database/db.js'))

const users = function (req, res, next) {
  if (req.user !== null && req.user !== undefined) {
    db('users')
      .where({
        id: req.user,
      })
      .select('name')
      .then((rows) => {
        if (rows[0] !== undefined) {
          res.end(JSON.stringify({ name: rows[0].name }))
        } else {
          res.statusCode = 500
          res.end(JSON.stringify({ message: 'Error.' }))
        }
      })
      .catch((err) => {
        res.statusCode = 500
        res.end(JSON.stringify({ message: `Error code: ${err.errno}.` }))
      })
  } else {
    res.statusCode = 401
    res.end(JSON.stringify({ message: 'Log in required.' }))
  }
}

module.exports = {
  handler(req, res, next) {
    if (req.method === 'GET') users(req, res, next)
  },
}
