export default {
  dev: process.env.NODE_ENV !== 'production',
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'login',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    'nuxt-windicss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },
  windicss: {
    jit: true,
    // add '~tailwind.config` alias
    exposeConfig: true,
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  serverMiddleware: [
    {
      path: '/api',
      handler: require('body-parser').raw(),
    },
    {
      path: '/api',
      handler: require('body-parser').text(),
    },
    {
      path: '/api',
      handler: require('body-parser').urlencoded(),
    },
    {
      path: '/api',
      handler: require('body-parser').json(),
    },
    {
      path: /^\/api\/(!signin|!signup).*$/,
      handler: '~/api/authenticate.js',
    },
    {
      path: '/api/signup',
      handler: '~/api/signup.js',
    },
    {
      path: '/api/signin',
      handler: '~/api/signin.js',
    },
    {
      path: '/api/users',
      handler: '~/api/users.js',
    },
  ],
}
