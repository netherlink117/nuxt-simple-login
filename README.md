# Nuxt-simple-login
This is an small project that I made just for learning Nuxt.js during my stream on Twitch, but due to some requeriments of usability and presentation, I had to learn also Knex.js (SQL query builder) and also give a look to WindiCSS. 
## Project setup
The examples from below use yarn, but npm can be used too.
First, SQLite and Knex.js must be initiated. Under the `api` folder there is the `database` folder where the SQLite database and migrations will be created, so run:
```bash
$ yarn global add knex
$ cd api/database
$ knex migrate:latest
```
Then all Nuxt.js dependencies are need to be installed. To install Nuxt.js dependencies run:
```bash
$ yarn install
```
Finally, there are multiple options to run, as described below:
```bash
# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```
For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
## License
The script's code shared on this repository is shared under [The Unlicense](https://unlicense.org) license.
